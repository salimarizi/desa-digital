@extends('layouts.front_layout')

@section('content')
  <div id="banner-area">
  	<img src="{!! asset('frontend_assets/images/banner/banner1.jpg') !!}" alt="" />
  	<div class="parallax-overlay"></div>
  	<!-- Subpage title start -->
  	<div class="banner-title-content">
  		<div class="text-center">
  			<h2>Layanan</h2>
  			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb justify-content-center">
  					<li class="breadcrumb-item"><a href="#">Home</a></li>
  					<li class="breadcrumb-item text-white" aria-current="page">Layanan</li>
  				</ol>
  			</nav>
  		</div>
  	</div><!-- Subpage title end -->
  </div><!-- Banner area end -->

  <!-- Main container start -->
  <section id="main-container">
  	<div class="container">
  		<!-- Services -->
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon classic float-left"><i class="fa fa-cogs"></i></span>
  				<h2 class="title classic">Layanan Kami</h2>
  			</div>
  		</div>
      @foreach ($services as $service)
        <div class="row">
          <div class="col-md-3 col-sm-3 mb-5 wow fadeInDown" data-wow-delay="1.4s">
    				<div class="service-content text-center">
    					<span class="service-icon icon-pentagon"><i class="fa fa-lightbulb-o"></i></span>
    				</div>
    			</div>
          <div class="col-md-8 col-sm-8 mb-5 wow fadeInDown" data-wow-delay="1.4s">
            <div class="service-content">
              <h3>{{ $service->name }}</h3>
    					<p>{{ $service->description }}</p>
            </div>
          </div>
    		</div>
      @endforeach
  		<!-- Services end -->
  	</div>
  	<!--/ 1st container end -->

  	<div class="gap-60"></div>

  	<!-- Testimonial start-->
    <div class="row">
      <div class="col-md-8">
        <div class="testimonial parallax parallax3">
      		<div class="parallax-overlay"></div>
      		<div class="container">
      			<div class="row">
      				<div id="testimonial-carousel" class="owl-carousel owl-theme text-center testimonial-slide">
                @foreach ($testimonies->chunk(3) as $chunk)
                  <div class="item">
                    @foreach ($chunk as $testimony)
                      <div class="row">
                        <div class="col-md-2">
                          <div class="testimonial-thumb">
                            <img src="{!! $testimony->image ? asset('storage/testimonies/'.$testimony->image) : asset('frontend_assets/images/team/testimonial1.jpg') !!}" alt="testimonial">
                          </div>
                        </div>
                        <div class="col-md-8 text-left">
                          <div class="row">
                            <div class="col-md-12">
                              <b>{{ $testimony->name }}</b>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                              <p>{{ $testimony->testimonies }}</p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <br>
                    @endforeach
                  </div>
                @endforeach
      				</div>
      				<!--/ Testimonial carousel end-->
      			</div>
      			<!--/ Row end-->
      		</div>
      		<!--/  Container end-->
      	</div>
      </div>
      <div class="col-md-4">
        <div class="row">
          <div class="col-md-12 heading">
            <span class="title-icon classic float-left"><i class="fa fa-users"></i></span>
            <h2 class="title classic">Mitra Kami</h2>
          </div>
        </div>
        <div class="row">
          @foreach ($partners as $partner)
            <div class="col-md-4" style="margin-top: 20px; text-align: center">
              @if ($partner->image)
                <img src="{{ url('storage/partners/'.$partner->image) }}" class="img img-fluid" alt="{{ $partner->name }}" title="{{ $partner->name }}" style="max-height: 100px;">
              @else
                <img src="{{ url('default_images/user.png') }}" class="img img-fluid" alt="{{ $partner->name }}" title="{{ $partner->name }}" style="max-height: 100px;">
              @endif
            </div>
          @endforeach
        </div>
      </div>
    </div>
  	<!--/ Testimonial end-->

  	<div class="gap-60"></div>

    <div class="container">
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon float-left"><i class="fa fa-university"></i></span>
  				<h2 class="title">Pilihan Paket <span class="title-desc">Kami menyediakan beberapa paket</span></h2>
  			</div>
  		</div><!-- Title row end -->
  		<div class="row">
        @foreach ($pricings as $pricing)
          <!-- plan start -->
          <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="1.4s">
            <div class="plan text-center featured">
              <span class="plan-name"><font size="4">{{ $pricing->title }}</font></span>
              <p class="plan-price">
                <sup class="currency">{{ $pricing->currency }}</sup>
                <strong style="font-size: 28pt">{{ number_format($pricing->price, 0, ',', '.') }}</strong>
              </p>
              @php
                $features = explode(',', $pricing->features);
              @endphp
              <ul class="list-unstyled">
                @foreach ($features as $feature)
                  <li>{{ $feature }}</li>
                @endforeach
              </ul>
              <a class="btn btn-primary" href="#.">Pilih</a>
            </div>
          </div><!-- plan end -->
        @endforeach
  		</div>
  		<!--/ Content row end -->
  	</div>
  </section>
  <!--/ Main container end -->

  <section class="call-to-action">
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-12">
          <h3>Bergabunglah sekarang dan rasakan mudahnya mengelola desa</h3>
  				<a href="#" class="float-right btn btn-primary white">Bergabung sekarang</a>
  			</div>
  		</div>
  	</div>
  </section>
@endsection
