@extends('layouts.front_layout')

@section('content')
  <div id="banner-area">
  	<img src="{!! asset('frontend_assets/images/banner/banner1.jpg') !!}" alt="" />
  	<div class="parallax-overlay"></div>
  	<!-- Subpage title start -->
  	<div class="banner-title-content">
  		<div class="text-center">
  			<h2>Konsep Digital</h2>
  			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb justify-content-center">
  					<li class="breadcrumb-item"><a href="#">Home</a></li>
  					<li class="breadcrumb-item text-white" aria-current="page">Konsep Digital</li>
  				</ol>
  			</nav>
  		</div>
  	</div><!-- Subpage title end -->
  </div><!-- Banner area end -->

  <!-- Main container start -->
  <section id="main-container">
  	<div class="container">
  		<!-- Services -->
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon classic float-left"><i class="fa fa-comments"></i></span>
  				<h2 class="title classic">Hal yang sering ditanyakan tentang konsep digital</h2>
  			</div>
  		</div>

  		<div class="row">
        @foreach ($faqs as $faq)
          <div class="col-md-6 col-sm-6">
            <div class="faq-box">
              <h4>Q. {{ $faq->question }}?</h4>
              <p>{{ $faq->answer }}.</p>
            </div>
          </div><!-- End col-md-6 -->
        @endforeach
  		</div><!-- Content row  end -->
  	</div>
  	<!--/ container end -->
  </section>
  <!--/ Main container end -->

  <section class="call-to-action">
  	<div class="container">
  		<div class="row">
  			<div class="col-sm-12">
  				<h3>Bergabunglah sekarang dan rasakan mudahnya mengelola desa</h3>
  				<a href="#" class="float-right btn btn-primary white">Bergabung sekarang</a>
  			</div>
  		</div>
  	</div>
  </section>
@endsection
