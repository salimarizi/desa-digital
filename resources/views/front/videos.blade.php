@extends('layouts.front_layout')

@section('content')
  <div id="banner-area">
  	<img src="{!! asset('frontend_assets/images/banner/banner1.jpg') !!}" alt="" />
  	<div class="parallax-overlay"></div>
  	<!-- Subpage title start -->
  	<div class="banner-title-content">
  		<div class="text-center">
  			<h2>Tutorial</h2>
  			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb justify-content-center">
  					<li class="breadcrumb-item"><a href="#">Home</a></li>
  					<li class="breadcrumb-item text-white" aria-current="page">Tutorial</li>
  				</ol>
  			</nav>
  		</div>
  	</div><!-- Subpage title end -->
  </div><!-- Banner area end -->

  <!-- Main container start -->
  <section id="main-container">
  	<div class="container">
  		<!-- Services -->
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon classic float-left"><i class="fa fa-comments"></i></span>
  				<h2 class="title classic">Tutorial penggunaan Aplikasi Desa Digital</h2>
  			</div>
  		</div>

  		<div class="row">
        @foreach ($videos as $video)
          <div class="col-md-4" style="background:#eee">
            <div class="row">
              <div class="col-md-12 text-center">
                <a href="{{ $video->link }}"><h4>{{ $video->name }}</h4></a>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 text-center">
                <iframe width="350" height="215" src="{{ 'https://www.youtube.com/embed/'.$video->code }}">
                </iframe>
              </div>
            </div>
          </div>
          <hr>
        @endforeach
  		</div><!-- Content row  end -->
  	</div>
  	<!--/ container end -->
  </section>
  <!--/ Main container end -->
@endsection
