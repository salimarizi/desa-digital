@extends('layouts.front_layout')
@section('content')
  <div id="banner-area">
  	<img src="{!! asset('frontend_assets/images/banner/banner1.jpg') !!}" alt="" />
  	<div class="parallax-overlay"></div>
  	<!-- Subpage title start -->
  	<div class="banner-title-content">
  		<div class="text-center">
  			<h2>Berita Desa</h2>
  			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb justify-content-center">
  					<li class="breadcrumb-item"><a href="#">Home</a></li>
  					<li class="breadcrumb-item text-white" aria-current="page">Berita Desa</li>
  				</ol>
  			</nav>
  		</div>
  	</div><!-- Subpage title end -->
  </div><!-- Banner area end -->

  <!-- Blog details page start -->
  <section id="main-container">
  	<div class="container">
  		<div class="row">

  			<!-- Blog start -->
  			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">

          @foreach ($news as $news_detail)
            <!-- 1st post start -->
            <div class="post">
              <!-- post image start -->
              <div class="post-image-wrapper">
                @if ($news_detail->image)
                  <img src="{!! asset('storage/news/'.$news_detail->image) !!}" class="img-fluid" alt="" />
                @else
                  <img src="{!! asset('frontend_assets/images/blog/blog1.jpg') !!}" class="img-fluid" alt="" />
                @endif
                <span class="blog-date"><a href="#"> {{ $news_detail->created_at->format('M d, Y') }}</a></span>
              </div><!-- post image end -->
              <div class="post-header clearfix">
                <h2 class="post-title">
                  <a href="{{ url('news-detail') }}">{{ $news_detail->title }}</a>
                </h2>
                <div class="post-meta">
                  <span class="post-meta-author">Posted by <a href="#"> Admin</a></span>
                  <span class="post-meta-cats">in <a href="#"> News</a></span>
                </div><!-- post meta end -->
              </div><!-- post heading end -->
              <div class="post-body">
                <p>{{ str_limit($news_detail->content, 100) }}</p>
                </div>
                <div class="post-footer">
                  <a href="{{ url('news-detail/'.$news_detail->id) }}" class="btn btn-primary">Lanjutkan Membaca <i
                    class="fa fa-angle-double-right">&nbsp;</i></a>
                  </div>
                </div><!-- 1st post end -->
          @endforeach

  				<nav>
  					<ul class="pagination">
  						<li class="page-item">
  							<a class="page-link" href="#" aria-label="Previous">
  								<i class="fa fa-angle-left"></i>
  							</a>
  						</li>
  						<li class="page-item active"><a class="page-link" href="#">1</a></li>
  						<li class="page-item"><a class="page-link" href="#">2</a></li>
  						<li class="page-item"><a class="page-link" href="#">3</a></li>
  						<li class="page-item"><a class="page-link" href="#">4</a></li>
  						<li class="page-item"><a class="page-link" href="#">5</a></li>
  						<li class="page-item">
  							<a class="page-link" href="#" aria-label="Next">
  								<i class="fa fa-angle-right"></i>
  							</a>
  						</li>
  					</ul>
  				</nav>
  			</div>
  			<!--/ Content col end -->

  			<!-- sidebar start -->
  			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

  				<div class="sidebar sidebar-right">

  					<!-- Blog search start -->
  					<div class="widget widget-search">
  						<h3 class="widget-title">Search</h3>
  						<div id="search">
  							<input class="form-control form-control-lg" placeholder="search" type="search">
  						</div>
  					</div><!-- Blog search end -->

  					<!-- Blog category start -->
  					<div class="widget widget-categories">
  						<h3 class="widget-title">Kategori Berita</h3>
  						<ul class="category-list clearfix">
                @foreach ($news_categories as $news_category)
                  <li><a href="#">{{ $news_category->name }}</a><span class="posts-count"> ({{ $news_category->news->count() }})</span></li>
                @endforeach
  						</ul>
  					</div><!-- Blog category end -->

  					<!-- Blog tags start -->
  					<div class="widget widget-tags">
  						<h3 class="widget-title">Popular Tags</h3>
  						<ul class="list-unstyled clearfix">
                @foreach ($tags as $tag)
                  <li><a href="#">{{ $tag->name }}</a></li>
                @endforeach
  						</ul>
  					</div><!-- Blog tags end -->

  					<!-- Blog tags start -->
  					<div class="widget">
  						<h3 class="widget-title">Berita Desa</h3>
  						<p>
                Halaman Berita Desa akan senantiasa memberikan berita terupdate tentang desa-desa yang menjadi
                mitra kami.
               </p>
  					</div><!-- Text widget end -->

  				</div><!-- sidebar end -->
  			</div>
  		</div>
  		<!--/ row end -->
  	</div>
  	<!--/ container end -->
  </section><!-- Blog details page end -->

  <div class="gap-40"></div>
@endsection
