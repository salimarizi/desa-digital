@extends('layouts.front_layout')
@section('content')
  <div id="banner-area">
  	<img src="{!! asset('frontend_assets/images/banner/banner1.jpg') !!}" alt="" />
  	<div class="parallax-overlay"></div>
  	<!-- Subpage title start -->
  	<div class="banner-title-content">
  		<div class="text-center">
  			<h2>Aplikasi Desa</h2>
  			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb justify-content-center">
  					<li class="breadcrumb-item"><a href="#">Home</a></li>
  					<li class="breadcrumb-item text-white" aria-current="page">Aplikasi Desa</li>
  				</ol>
  			</nav>
  		</div>
  	</div><!-- Subpage title end -->
  </div><!-- Banner area end -->

  <!-- Portfolio item start -->
  <section id="portfolio-item">
  	<div class="container">
  		<!-- Portfolio item row start -->
      @foreach ($apps as $app)
        <div class="row">
          <!-- Portfolio item slider start -->
          <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
            <div class="portfolio-slider">
              <div class="flexportfolio flexslider">
                <ul class="slides">
                  @if (count($app->images))
                    @foreach ($app->images as $image)
                      <li><img src="{!! asset('storage/apps/'.$image->image) !!}" alt="Gambar Aplikasi"></li>
                    @endforeach
                  @else
                    <li><img src="{!! asset('frontend_assets/images/portfolio/portfolio-bg1.jpg') !!}" alt=""></li>
                  @endif
                </ul>
              </div>
            </div>
          </div>
          <!-- Portfolio item slider end -->

          <!-- sidebar start -->
          <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
            <div class="sidebar">
              <div class="portfolio-desc">
                <h3 class="widget-title">{{ $app->name }}</h3>
                <p>
                  {{ $app->description }}
                </p>
                <br />
                <h3 class="widget-title">Fitur</h3>
                <p>{{ $app->feature }}</p>
                <br />
                <h3 class="widget-title">Client</h3>
                <p>{{ $app->client }}</p>
                <p><a href="{{ $app->link }}" class="project-btn btn btn-primary">Link Aplikasi</a></p>
              </div>
            </div>
          </div>
          <!-- sidebar end -->
        </div><!-- Portfolio item row end -->

        <hr>
      @endforeach
  	</div><!-- Container end -->
  </section><!-- Portfolio item end -->

  <div class="gap-40"></div>
@endsection
