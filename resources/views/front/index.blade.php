@extends('layouts.front_layout')

@section('content')
  <!-- Slider start -->
  <section id="home" class="p-0">
  	<div id="main-slide" class="cd-hero">
  		<ul class="cd-hero-slider">
  			<li class="selected">
  				<div class="overlay2">
            @if ($slider_1)
              <img class="" src="{!! asset('storage/settings/'.$slider_1) !!}" alt="slider">
            @else
              <img class="" src="{!! asset('frontend_assets/images/slider/bg1.jpg') !!}" alt="slider">
            @endif
  				</div>
  				<div class="cd-full-width">
  					<h2>Menuju Masa Depan!</h2>
  					<h3>Kami membuat sistem untuk mewujudkan Good and Clean Governance</h3>
  					<a href="#0" class="btn btn-primary white cd-btn">Start Now</a>
  					<a href="#0" class="btn btn-primary solid cd-btn">Learn More</a>
  				</div> <!-- .cd-full-width -->
  			</li>
  			<li>
  				<div class="overlay2">
            @if ($slider_2)
              <img class="" src="{!! asset('storage/settings/'.$slider_2) !!}" alt="slider">
            @else
              <img class="" src="{!! asset('frontend_assets/images/slider/bg2.jpg') !!}" alt="slider">
            @endif
  				</div>
  				<div class="cd-half-width">
  					<h2>Wujudkan Impian Desa</h2>
  					<p>Kami berusaha membangun desa dengan mewujudkan impian dan cita-cita desa</p>
  					<a href="#0" class="cd-btn btn btn-primary solid">Mulai Bermimpi</a>
  				</div> <!-- .cd-half-width -->

  				<div class="cd-half-width cd-img-container">
  					<img src="{!! asset('frontend_assets/images/slider/bg-thumb1.png') !!}" alt="">
  				</div> <!-- .cd-half-width.cd-img-container -->
  			</li>
  			<li>
  				<div class="overlay2">
            @if ($slider_3)
              <img class="" src="{!! asset('storage/settings/'.$slider_3) !!}" alt="slider">
            @else
              <img class="" src="{!! asset('frontend_assets/images/slider/bg3.jpg') !!}" alt="slider">
            @endif
  				</div>
  				<div class="cd-half-width cd-img-container img-right">
  					<img src="{!! asset('frontend_assets/images/slider/bg-thumb2.png') !!}" alt="">
  				</div> <!-- .cd-half-width.cd-img-container -->
  				<div class="cd-half-width">
  					<h2>Keinginan Desa adalah Prioritas</h2>
  					<p>Keinginan warga desa menjadi prioritas pelayanan yang kami berikan</p>
  					<a href="#0" class="cd-btn btn btn-primary white">Start</a>
  					<a href="#0" class="cd-btn btn secondary btn-primary solid">Learn More</a>
  				</div> <!-- .cd-half-width -->
  			</li>
        <li>
  				<div class="overlay2">
            @if ($slider_4)
              <img class="" src="{!! asset('storage/settings/'.$slider_4) !!}" alt="slider">
            @else
              <img class="" src="{!! asset('frontend_assets/images/slider/bg3.jpg') !!}" alt="slider">
            @endif
  				</div>

          <div class="cd-half-width">
            <h2>KAMI DISINI UNTUK MENYATUKAN</h2>
  					<p>Kami membuat sistem yang menyatukan warga dan keperluan desa dan mewujudkannya</p>
  					<a href="#0" class="cd-btn btn btn-primary solid">Ayo bergabung</a>
  				</div> <!-- .cd-half-width -->

  				<div class="cd-half-width cd-img-container">
  					<img src="{!! asset('frontend_assets/images/slider/bg-thumb1.png') !!}" alt="">
  				</div> <!-- .cd-half-width.cd-img-container -->
  			</li>
  		</ul>
  		<!--/ cd-hero-slider -->

  		<div class="cd-slider-nav">
  			<nav>
  				<span class="cd-marker item-1"></span>
  				<ul>
  					<li class="selected"><a href="#0"><i class="fa fa-bicycle"></i> Invent</a></li>
  					<li><a href="#0"><i class="fa fa-hotel"></i> Dream</a></li>
  					<li><a href="#0"><i class="fa fa-android"></i> Tech</a></li>
  					<li class="video"><a href="#0"><i class="fa fa-video-camera"></i> Video</a></li>
  				</ul>
  			</nav>
  		</div> <!-- .cd-slider-nav -->

  	</div>
  	<!--/ Main slider end -->
  </section>
  <!--/ Slider end -->


  <!-- Service box start -->
  <section id="service" class="service angle">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon float-left"><i class="fa fa-cogs"></i></span>
  				<h2 class="title">Layanan yang Kami Berikan
            <span class="title-desc">
              Kami disini untuk melayani semua kebutuhan desa
            </span>
          </h2>
  			</div>
  		</div><!-- Title row end -->

  		<div class="row">
        @foreach ($services as $service)
          <div class="col-md-3 col-sm-3 wow fadeInDown" data-wow-delay=".5s">
            <div class="service-content text-center">
              <span class="service-icon icon-pentagon"><i class="fa fa-android"></i></span>
              <h3>{{ $service->name }}</h3>
              <p>{{ $service->description }}</p>
            </div>
          </div>
        @endforeach
  		</div><!-- Content row end -->
  	</div>
  	<!--/ Container end -->
  </section>
  <!--/ Service box end -->

  <!-- Portfolio start -->
  <section id="portfolio" class="portfolio">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon classic float-left"><i class="fa fa-suitcase"></i></span>
  				<h2 class="title classic">Produk Kami</h2>
  			</div>
  		</div> <!-- Title row end -->
  	</div>

  	<div class="container-fluid">
  		<div class="row isotope" id="isotope">
        @foreach ($apps as $app)
          <div class="col-sm-2 web-design isotope-item">
    				<div class="grid">
    					<figure class="m-0 effect-oscar">
                @if ($app->image)
                  <li><img src="{!! asset('storage/apps/'.$app->image->image) !!}" alt="Gambar Aplikasi"></li>
                @else
                  <img src="{!! asset('frontend_assets/images/portfolio/portfolio1.jpg') !!}" alt="">
                @endif
    						<figcaption>
    							<h3>{{ $app->name }}</h3>
    							<a class="link icon-pentagon" href="{{ url('apps/'.$app->id) }}">
                    <i class="fa fa-link"></i>
                  </a>
    						</figcaption>
    					</figure>
    				</div>
    			</div>
        @endforeach
  		</div><!-- Content row end -->
  	</div><!-- Container end -->
  </section><!-- Portfolio end -->

  <!-- Counter Strat -->
  <section class="ts_counter p-0">
  	<div class="container-fluid">
  		<div class="row facts-wrapper wow fadeInLeft text-center">
  			<div class="facts one col-md-3 col-sm-6">
  				<span class="facts-icon"><i class="fa fa-user"></i></span>
  				<div class="facts-num">
  					<span class="counter">{{ $desa_mitra }}</span>
  				</div>
  				<h3>DESA MITRA</h3>
  			</div>

  			<div class="facts two col-md-3 col-sm-6">
  				<span class="facts-icon"><i class="fa fa-institution"></i></span>
  				<div class="facts-num">
  					<span class="counter">{{ $bumdes_mitra }}</span>
  				</div>
  				<h3>BUMDES MITRA</h3>
  			</div>

  			<div class="facts three col-md-3 col-sm-6">
  				<span class="facts-icon"><i class="fa fa-suitcase"></i></span>
  				<div class="facts-num">
  					<span class="counter">{{ $proyek_desa }}</span>
  				</div>
  				<h3>PROYEK DESA</h3>
  			</div>

  			<div class="facts four col-md-3 col-sm-6">
  				<span class="facts-icon"><i class="fa fa-trophy"></i></span>
  				<div class="facts-num">
  					<span class="counter">{{ $petani_binaan }}</span>
  				</div>
  				<h3>PETANI BINAAN</h3>
  			</div>

  		</div>
  	</div>
  	<!--/ Container end -->
  </section>
  <!--/ Counter end -->

  <!-- Feature box start -->
  <section id="feature" class="feature">
    <div class="container">
  		<div class="row">
  			<div class="col-md-12 heading">
  				{{-- <span class="title-icon classic float-left"><i class="fa fa-suitcase"></i></span> --}}
  				<h2 class="title classic">Fitur SIFADU</h2>
  			</div>
  		</div> <!-- Title row end -->
  	</div>
  	<div class="container">
  		<div class="row">
        @foreach ($features as $feature)
          <div class="feature-box col-sm-4 wow fadeInDown" data-wow-delay=".5s">
    				<span class="feature-icon float-left">
              <img src="{{ url('storage/features/'.$feature->image) }}" class="img img-fluid" style="max-width: 50px">
            </span>
    				<div class="feature-content">
    					<h3>{{ $feature->name }}</h3>
    					<p>{{ $feature->description }}</p>
    				</div>
    			</div>
        @endforeach
  			<!--/ End first featurebox -->
  		</div><!-- Content row end -->

  	</div>
  	<!--/ Container end -->
  </section>
  <!--/ Feature box end -->


  <section id="image-block" class="image-block p-0">
  	<div class="container-fluid">
  		<div class="row">
  			<div class="col-md-5">
          <div class="row">
            <div class="col-md-12" style="margin-top: 20px">
              <h2 class="title">Berita Desa
                <span class="title-desc">
                  Halaman Berita Desa akan senantiasa memberikan berita terupdate tentang desa-desa yang menjadi
                  mitra kami.<br><br>
                </span>
              </h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              @if ($news[0]->image)
                <img src="{!! asset('storage/news/'.$news[0]->image) !!}" class="img-fluid" alt="" />
              @else
                <img src="{!! asset('frontend_assets/images/blog/blog1.jpg') !!}" class="img-fluid" alt="" />
              @endif
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h2 class="post-title">
  							<a href="{{ url('news-detail/'.$news[0]->id) }}">{{ $news[0]->title }}</a>
  						</h2>
            </div>
          </div>
  			</div>
  			<div class="col-md-7 ts-padding img-block-right">
  				<div class="img-block-head">
  					<h2>Berita Lain</h2>
  				</div>

  				<div class="gap-30"></div>

          @foreach ($news->slice(1, 3) as $news_detail)
            <div class="image-block-content" style="margin: 5px">
              <div class="row">
                <div class="col-md-5">
                  @if ($news_detail->image)
                    <img src="{!! asset('storage/news/'.$news_detail->image) !!}" class="img-fluid" alt="" />
                  @else
                    <img src="{!! asset('frontend_assets/images/blog/blog1.jpg') !!}" class="img-fluid" alt="" />
                  @endif
                </div>
                <div class="col-md-7">
                  <a href="{{ url('news-detail/'.$news_detail->id) }}">
                    <h5>{{ $news_detail->title }}</h5>
                  </a>
      						<p>{{ str_limit($news_detail->content, 60) }}</p>
                </div>
              </div>
    				</div>
          @endforeach
  				<!--/ End 1st block -->

  			</div>
  		</div>
  	</div>
  </section>
  <!--/ Image block end -->

  <!-- Parallax 1 start -->
  <section class="parallax parallax1" style="padding: 50px !important">
  	<div class="parallax-overlay"></div>
  	<div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              &nbsp;
            </div>
          </div>
          @foreach ($videos->slice(1, 3) as $video)
            <div class="row">
              <div class="col-md-6">
                <iframe width="100%" height="160" src="{{ 'https://www.youtube.com/embed/'.$video->code }}">
                </iframe>
              </div>
              <div class="col-md-6">
                <div class="row">
                  <div class="col-md-12">
                    <a href="{{ $video->link }}"><h4>{{ $video->name }}</h4></a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <p>{{ $video->description }}</p>
                  </div>
                </div>
              </div>
            </div>
          @endforeach
        </div>
        <div class="col-md-6">
          <div class="row">
            <div class="col-md-12">
              <a href="{{ $videos[0]->link }}"><h2>{{ $videos[0]->name }}</h2></a>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <iframe width="100%" height="410" src="{{ 'https://www.youtube.com/embed/'.$videos[0]->code }}">
              </iframe>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h4>Caption</h4>
              {{ $videos[0]->description }}
            </div>
          </div>
        </div>
  		</div>
  	</div><!-- Container end -->
  </section><!-- Parallax 1 end -->

  <!-- Pricing table start -->
  <section id="pricing" class="pricing">
  	<div class="container">
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon float-left"><i class="fa fa-university"></i></span>
  				<h2 class="title">Pilihan Paket <span class="title-desc">Kami menyediakan beberapa paket</span></h2>
  			</div>
  		</div><!-- Title row end -->
  		<div class="row">
        @foreach ($pricings as $pricing)
          <!-- plan start -->
          <div class="col-md-3 col-sm-6 wow fadeInUp" data-wow-delay="1.4s">
            <div class="plan text-center featured">
              <span class="plan-name"><font size="4">{{ $pricing->title }}</font></span>
              <p class="plan-price">
                <sup class="currency">{{ $pricing->currency }}</sup>
                <strong style="font-size: 28pt">{{ number_format($pricing->price, 0, ',', '.') }}</strong>
              </p>
              @php
                $features = explode(',', $pricing->features);
              @endphp
              <ul class="list-unstyled">
                @foreach ($features as $feature)
                  <li>{{ $feature }}</li>
                @endforeach
              </ul>
              <a class="btn btn-primary" href="#.">Pilih</a>
            </div>
          </div><!-- plan end -->
        @endforeach
  		</div>
  		<!--/ Content row end -->
  	</div>
  	<!--/  Container end-->
  </section>
  <!--/ Pricing table end -->


  <!-- Testimonial start-->
  <div class="row">
    <div class="col-md-8">
      <section class="testimonial parallax parallax2" style="min-height: 500px;">
      	<div class="parallax-overlay"></div>
      	<div class="container">
      		<div class="row">
      			<div id="testimonial-carousel" class="owl-carousel owl-theme text-center testimonial-slide">
              @foreach ($testimonies->chunk(3) as $chunk)
                <div class="item">
                  @foreach ($chunk as $testimony)
                    <div class="row">
                      <div class="col-md-2">
                        <div class="testimonial-thumb">
                          <img src="{!! $testimony->image ? asset('storage/testimonies/'.$testimony->image) : asset('frontend_assets/images/team/testimonial1.jpg') !!}" alt="testimonial">
                        </div>
                      </div>
                      <div class="col-md-8 text-left">
                        <div class="row">
                          <div class="col-md-12">
                            <b>{{ $testimony->name }}</b>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <p>{{ $testimony->testimonies }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br>
                  @endforeach
                  {{-- <div class="testimonial-thumb">
                    <img src="{!! $testimony->image ? asset('storage/testimonies/'.$testimony->image) : asset('frontend_assets/images/team/testimonial1.jpg') !!}" alt="testimonial">
                  </div>
                  <div class="testimonial-content">
                    <p class="testimonial-text">
                      {{ $testimony->testimonies }}
                    </p>
                    <h3 class="name">
                      {{ $testimony->name }}
                      <span>Mitra Desa Digital</span>
                    </h3>
                  </div> --}}
                </div>
              @endforeach
      			</div>
      			<!--/ Testimonial carousel end-->
      		</div>
      		<!--/ Row end-->
      	</div>
      	<!--/  Container end-->
      </section>
    </div>
    <div class="col-md-4">
      <div class="row">
        <div class="col-md-12 heading">
          <span class="title-icon classic float-left"><i class="fa fa-users"></i></span>
          <h2 class="title classic">Mitra Kami</h2>
        </div>
      </div>
      <div class="row">
        @foreach ($partners as $partner)
          <div class="col-md-4" style="margin-top: 20px; text-align: center">
            @if ($partner->image)
              <img src="{{ url('storage/partners/'.$partner->image) }}" class="img img-fluid" alt="{{ $partner->name }}" title="{{ $partner->name }}" style="max-height: 100px;">
            @else
              <img src="{{ url('default_images/user.png') }}" class="img img-fluid" alt="{{ $partner->name }}" title="{{ $partner->name }}" style="max-height: 100px;">
            @endif
          </div>
        @endforeach
      </div>
    </div>
  </div>
  <!--/ Testimonial end-->
@endsection
