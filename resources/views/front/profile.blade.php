@extends('layouts.front_layout')
@section('content')
  <div id="banner-area">
  	<img src="{!! asset('frontend_assets/images/banner/banner1.jpg') !!}" alt="" />
  	<div class="parallax-overlay"></div>
  	<!-- Subpage title start -->
  	<div class="banner-title-content">
  		<div class="text-center">
  			<h2>Profile</h2>
  			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb justify-content-center">
  					<li class="breadcrumb-item"><a href="#">Home</a></li>
  					<li class="breadcrumb-item text-white" aria-current="page">Profile</li>
  				</ol>
  			</nav>
  		</div>
  	</div><!-- Subpage title end -->
  </div><!-- Banner area end -->

  <!-- Main container start -->
  <section id="main-container">
  	<div class="container">

  		<!-- Company Profile -->
  		<div class="row">
  			<div class="col-md-12 heading">
  				<span class="title-icon classic float-left"><i class="fa fa-suitcase"></i></span>
  				<h2 class="title classic">Company Profile</h2>
  			</div>
  		</div><!-- Title row end -->

  		<div class="row landing-tab">
  			<div class="col-md-3 col-sm-5">
  				<div class="nav flex-column nav-pills border-right" id="v-pills-tab" role="tablist" aria-orientation="vertical">
  					<a class="animated fadeIn nav-link py-4 active d-flex align-items-center" data-toggle="pill" href="#tab_1"
  						role="tab" aria-selected="true">
  						<i class="fa fa-info-circle mr-4"></i>
  						<span class="h4 mb-0 font-weight-bold">Who Are We</span>
  					</a>
  					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_2" role="tab"
  						aria-selected="true">
  						<i class="fa fa-briefcase mr-4"></i>
  						<span class="h4 mb-0 font-weight-bold">OUR COMPANY</span>
  					</a>
  					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_3" role="tab"
  						aria-selected="true">
  						<i class="fa fa-android mr-4"></i>
  						<span class="h4 mb-0 font-weight-bold">What We Do</span>
  					</a>
  					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_4" role="tab"
  						aria-selected="true">
  						<i class="fa fa-pagelines mr-4"></i>
  						<span class="h4 mb-0 font-weight-bold">Modern Design</span>
  					</a>
  					<a class="animated fadeIn nav-link py-4 d-flex align-items-center" data-toggle="pill" href="#tab_5" role="tab"
  						aria-selected="true">
  						<i class="fa fa-support mr-4"></i>
  						<span class="h4 mb-0 font-weight-bold">Dedicated Support</span>
  					</a>
  				</div>
  			</div>
  			<div class="col-md-9 col-sm-7">
  				<div class="tab-content" id="v-pills-tabContent">
  					<div class="tab-pane pl-sm-5 fade show active animated fadeInLeft" id="tab_1" role="tabpanel">
  						<i class="fa fa-trophy icon-xl text-primary mb-4"></i>
  						<h3>Apa itu Desa Digital?</h3>
  						<p>
                Desa Digital adalah suatu konsep tentang pengembangan desa dengan
                memanfaatkan teknologi digital baik dalam pelayanan publik maupun
                pengembangan kawasan seperti infrastruktur, Sistem Informasi, tata ruang,
                Agrobisnis hingga energi alternatif.<br/><br/>

                Konsep yang meng-integrasikan segala aktivitas utama masyarakat
                dengan teknologi berbasis Internet Of things ( IOT ) ini diharapkan
                mampu untuk menurunkan biaya ( cost reduction ) birokrasi dan meningkatkan
                pelayanan publik serta taraf hidup masyarakat pedesaan.<br/><br/>

                Karena melalui Desa Digital ini kinerja pemerintah desa lebih cepat, produktif dan
                efisien, pembangunan pun lebih terarah dan implikatif terencana dengan baik
              </p>
  					</div>
  					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_2" role="tabpanel">
  						<i class="fa fa-briefcase icon-xl text-primary mb-4"></i>
  						<h3>Bisnis Kepentingan Desa</h3>
  						<p>
                Kami disini berusaha sebaik mungkin untuk memenuhi kebutuhan desa dalam sisi teknologi
                agar desa-desa di Indonesia dapat berkembang dan mengikuti perkembangan teknologi yang
                sangat pesat.
              </p>
  					</div>
  					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_3" role="tabpanel">
  						<i class="fa fa-android icon-xl text-primary mb-4"></i>
  						<h3>Memberikan yang terbaik untuk Desa</h3>
  						<p>
                Memberikan pelayanan yang terbaik untuk Desa adalah kepentingan kami. Kami ingin mencapai:
                <br>
                <ol style="margin-left: 0">
                  <li>Tercapainya efektivitas dan efisiensi penyelenggaraan tata naskah dinas</li>
                  <li>Menghindari terjadinya tumpang tindih, salah tafsir, dan pemborosan penyelenggaraan
                  tata naskah dinas</li>
                  <li>Terwujudnya keterpaduan pengelolaan tata naskah dinas dengan unsur lainnya dalam
                  lingkup administrasi umum.</li>
                  <li>Terciptanya birokrasi modern yang efektif dan efisien di intansi pemerintah daerah dan
                  intansi aparat kelurahan / desa</li>
                </ol>
              </p>
  					</div>
  					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_4" role="tabpanel">
  						<i class="fa fa-pagelines icon-xl text-primary mb-4"></i>
  						<h3>Desain teknologi yang modern</h3>
  						<p>Demi kemudahan dan efektivitas pelayanan desa. Kami menyediakan
                desain yang user friendly dan dapat dengan mudah diimplementasikan
                untuk pelayanan kebutuhan warga desa</p>
  					</div>
  					<div class="tab-pane pl-sm-5 fade animated fadeInLeft" id="tab_5" role="tabpanel">
  						<i class="fa fa-support icon-xl text-primary mb-4"></i>
  						<h3>Support Penuh</h3>
  						<p>Kami memberikan dukungan penuh dengan menyelenggarakan BIMTEK dan
                pelatihan serta pendampingan selama 1 tahun</p>
  					</div>
  				</div>
  			</div>
  		</div>
  		<!--/ Content row end -->
  	</div>
  	<!--/ 1st container end -->


  	<div class="gap-60"></div>


  	<!-- Counter Strat -->
  	<div class="ts_counter_bg parallax parallax2">
  		<div class="parallax-overlay"></div>
  		<div class="container">
  			<div class="row wow fadeInLeft text-center">
  				<div class="col-12 text-center">
            <a href="{{ url('apps') }}" class="btn btn-primary solid">Lihat Aplikasi Kami</a>
          </div>
  			</div>
  			<!--/ row end -->
  		</div>
  		<!--/ Container end -->
  	</div>
  	<!--/ Counter end -->
  </section>
  <!--/ Main container end -->
@endsection
