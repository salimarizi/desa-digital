@extends('layouts.front_layout')
@section('content')
  <div id="banner-area">
  	<img src="{!! asset('frontend_assets/images/banner/banner1.jpg') !!}" alt="" />
  	<div class="parallax-overlay"></div>
  	<!-- Subpage title start -->
  	<div class="banner-title-content">
  		<div class="text-center">
  			<h2>Kontak Kami</h2>
  			<nav aria-label="breadcrumb">
  				<ol class="breadcrumb justify-content-center">
  					<li class="breadcrumb-item"><a href="#">Home</a></li>
  					<li class="breadcrumb-item text-white" aria-current="page">Kontak Kami</li>
  				</ol>
  			</nav>
  		</div>
  	</div><!-- Subpage title end -->
  </div><!-- Banner area end -->

  <!-- Main container start -->
  <section id="main-container">
  	<div class="container">
  		<!-- Map start here -->
  		<div class="map" id="map_canvas" data-latitude="-6.861554" data-longitude="107.5649173" data-marker="{!! asset('frontend_assets/images/marker.png') !!}"></div>
  		<!--/ Map end here -->

  		<div class="gap-40"></div>

  		<div class="row">
  			<div class="col-md-7">
  				<form id="contact-form" action="contact-form.php" method="post" role="form">
  					<div class="row">
  						<div class="col-md-4">
  							<div class="form-group">
  								<label>Name</label>
  								<input class="form-control" name="name" id="name" placeholder="" type="text" required>
  							</div>
  						</div>
  						<div class="col-md-4">
  							<div class="form-group">
  								<label>Email</label>
  								<input class="form-control" name="email" id="email" placeholder="" type="email" required>
  							</div>
  						</div>
  						<div class="col-md-4">
  							<div class="form-group">
  								<label>Subject</label>
  								<input class="form-control" name="subject" id="subject" placeholder="" required>
  							</div>
  						</div>
  					</div>
  					<div class="form-group">
  						<label>Message</label>
  						<textarea class="form-control" name="message" id="message" placeholder="" rows="10" required></textarea>
  					</div>
  					<div class="text-right"><br>
  						<button class="btn btn-primary solid blank" type="submit">Send Message</button>
  					</div>
  				</form>
  			</div>
  			<div class="col-md-5">
  				<div class="contact-info">
  					<h3>Detail Kontak</h3>
  					<p>Untuk informasi lebih lanjut Anda dapat menghubungi kami di</p>
  					<br>
  					<p><i class="fa fa-home info"></i> Jl. Sariwangi 3</p>
  					<p><i class="fa fa-phone info"></i> +(62) 22-220222 </p>
  					<p><i class="fa fa-envelope-o info"></i> admin@desadigital.org</p>
  					<p><i class="fa fa-globe info"></i> www.desadigital.com</p>
  				</div>
  			</div>
  		</div>
  	</div>
  	<!--/ container end -->
  </section>
  <!--/ Main container end -->
@endsection
