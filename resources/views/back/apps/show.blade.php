@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Aplikasi
@endsection

@section('modal')
  <div class="modal" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Gambar</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('admin/apps') }}" method="post" id="form-delete">
            @csrf
            {{ method_field('DELETE') }}
            Apakah Anda yakin ingin menghapus gambar ini
            <b><span id="image_name"></span></b>?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-danger">
              Hapus
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Aplikasi</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Nama Aplikasi </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $app->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Feature </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $app->feature }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Deskripsi </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $app->description }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Klien </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $app->client }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Link </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $app->link }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Daftar Gambar</h4>
                  </div>
                  <div class="col-md-2">
                    <a href="{{ url('admin/apps/'.$app->id.'/add_image') }}" class="btn btn-primary">
                      <i class="fa fa-plus"></i> Tambah Gambar
                    </a>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Gambar</th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('admin/apps/images/'.$app->id) }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'image', name: 'image' },
                { data: 'delete', name: 'delete', orderable: false, searchable: false }
            ]
        })
    })

    deleteModal = (id, name) => {
      $('#modal-delete #image_name').text(name)
      $('#modal-delete #form-delete').attr('action', "{{ url('admin/apps') }}/{{ $app->id }}/image/" + id)
    }
  </script>
@endsection
