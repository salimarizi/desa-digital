@extends('layouts.backoffice_layout')

@section('title')
  Aplikasi
@endsection

@section('sub_title')
  Tambah Aplikasi
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Aplikasi</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/apps') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Nama Aplikasi: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama Aplikasi" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Fitur: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('feature') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="feature" value="{{ old('feature') }}" placeholder="Fitur" required>
                          <small class="text-danger">{{ $errors->first('feature') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Deskripsi: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <textarea class="form-control" name="description" placeholder="Deskripsi">{{ old('description') }}</textarea>
                          <small class="text-danger">{{ $errors->first('description') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Klien: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('client') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="client" value="{{ old('client') }}" placeholder="Klien" required>
                          <small class="text-danger">{{ $errors->first('client') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Link: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="link" value="{{ old('link') }}" placeholder="Link" required>
                          <small class="text-danger">{{ $errors->first('link') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
