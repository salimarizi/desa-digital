@extends('layouts.backoffice_layout')

@section('title')
  Aplikasi
@endsection

@section('sub_title')
  List Aplikasi
@endsection

@section('modal')
  <div class="modal" id="modal-delete">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Hapus Aplikasi</h4>
        </div>
        <div class="modal-body">
          <form action="{{ url('admin/apps') }}" method="post" id="form-delete">
            @csrf
            {{ method_field('DELETE') }}
            Apakah Anda yakin ingin menghapus aplikasi :
            <b><span id="apps_name"></span></b>?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">
              Close
            </button>
            <button type="submit" class="btn btn-danger">
              Hapus
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Daftar Aplikasi</h4>
                  </div>
                  <div class="col-md-2">
                    <a href="{{ url('admin/apps/create') }}" class="btn btn-primary">
                      <i class="fa fa-plus"></i> Tambah Aplikasi
                    </a>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <table id="table" class="table table-bordered table-striped table-hover">
                  <thead>
                    <tr>
                      <th>No.</th>
                      <th>Nama</th>
                      <th>Fitur</th>
                      <th>Deskripsi</th>
                      <th>Klien</th>
                      <th>Link</th>
                      <th></th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                </table>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script type="text/javascript">
    $(function() {
        $('#table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ url('admin/apps/data') }}",
            columns: [
                { data: 'DT_RowIndex', orderable: false, searchable: false },
                { data: 'name', name: 'name' },
                { data: 'feature', name: 'feature' },
                { data: 'description', name: 'description' },
                { data: 'client', name: 'client' },
                { data: 'link', name: 'link' },
                { data: 'edit', name: 'edit', orderable: false, searchable: false },
                { data: 'delete', name: 'delete', orderable: false, searchable: false },
                { data: 'show', name: 'show', orderable: false, searchable: false },
            ]
        })
    })

    deleteModal = (id, name) => {
      $('#modal-delete #apps_name').text(name)
      $('#modal-delete #form-delete').attr('action', "{{ url('admin/apps') }}/" + id)
    }
  </script>
@endsection
