@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Layanan
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Layanan</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  {{-- <div class="col-md-4">
                    @if ($service->image)
                      <img src="{{ url('storage/users/'.$user->image) }}" class="img img-responsive">
                    @else
                      <img src="{{ url('default_images/user.png') }}" class="img img-responsive">
                    @endif
                  </div> --}}
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Nama Layanan </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $service->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Deskripsi </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $service->description }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection
