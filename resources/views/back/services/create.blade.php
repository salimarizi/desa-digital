@extends('layouts.backoffice_layout')

@section('title')
  Layanan
@endsection

@section('sub_title')
  Tambah Layanan
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Layanan</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/services') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Nama Layanan: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama Layanan" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Deskripsi: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <textarea class="form-control" name="description" placeholder="Deskripsi">{{ old('description') }}</textarea>
                          <small class="text-danger">{{ $errors->first('description') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
