@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Kategori Berita
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Kategori Berita</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  {{-- <div class="col-md-4">
                    @if ($news_category->image)
                      <img src="{{ url('storage/users/'.$news_category->image) }}" class="img img-responsive">
                    @else
                      <img src="{{ url('default_images/user.png') }}" class="img img-responsive">
                    @endif
                  </div> --}}
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Nama </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $news_category->name }}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection
