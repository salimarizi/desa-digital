@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/fullcalendar/fullcalendar.min.css') !!}">
@endsection

@section('title')
  Dashboard
@endsection

@section('sub_title')
  Dashboard
@endsection

@section('content')
      <h1 align="center">Hi, {{ Auth::user()->name }}</h1>

      <div class="row">
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner" style="text-align: center">
              <h3>{{ count($partners) }}</h3>
              <p>Total Mitra</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{ url('admin/partners') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner" style="text-align: center">
              <h3>{{ count($testimonies) }}</h3>
              <p>Total Testimoni</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{ url('admin/testimonies') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner" style="text-align: center">
              <h3>{{ count($apps) }}</h3>
              <p>Total Aplikasi</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{ url('admin/apps') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Dashboard</h4>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                      <div class="inner" style="text-align: center">
                        <h3>Aktivitas</h3>
                        <p>Aktivitas Kami</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <a href="{{ url('admin/activities') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>

                  <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                      <div class="inner" style="text-align: center">
                        <h3>Video</h3>
                        <p>Video Kami</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-camera"></i>
                      </div>
                      <a href="{{ url('admin/videos') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>

                  <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                      <div class="inner" style="text-align: center">
                        <h3>Fitur</h3>
                        <p>Fitur yang kami miliki</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-tag"></i>
                      </div>
                      <a href="{{ url('admin/features') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>

                  <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-primary">
                      <div class="inner" style="text-align: center">
                        <h3>Pengaturan</h3>
                        <p>Pengaturan slider dan lainnya</p>
                      </div>
                      <div class="icon">
                        <i class="fa fa-gears"></i>
                      </div>
                      <a href="{{ url('admin/settings') }}" class="small-box-footer">Lihat Detail <i class="fa fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script src="{!! asset('admin_layouts/plugins/fullcalendar/moment.min.js') !!}"></script>
  <script src="{!! asset('admin_layouts/plugins/fullcalendar/fullcalendar.min.js') !!}"></script>

  <script type="text/javascript">
  </script>
@endsection
