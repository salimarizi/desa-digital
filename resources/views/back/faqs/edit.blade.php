@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Edit FAQ
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Edit FAQ</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/faqs/'.$faq->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                {{ method_field('PATCH') }}
                <div class="row">
                  <div class="col-md-2">
                    <label>Pertanyaan: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('question') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="question" value="{{ old('question') ? old('question') : $faq->question }}" placeholder="Pertanyaan" required>
                          <small class="text-danger">{{ $errors->first('question') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Jawaban: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('answer') ? ' has-error' : '' }}">
                          <textarea class="form-control" name="answer" placeholder="Jawaban">{{ old('answer') ? old('answer') : $faq->answer }}</textarea>
                          <small class="text-danger">{{ $errors->first('answer') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Edit</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
