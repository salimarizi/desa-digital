@extends('layouts.backoffice_layout')

@section('title')
  Tag
@endsection

@section('sub_title')
  Tambah Tag
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Tag</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/tags') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Nama: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="Nama" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
