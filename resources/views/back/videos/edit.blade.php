@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Edit Video
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Edit Video</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/videos/'.$video->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                {{ method_field('PATCH') }}
                <div class="row">
                  <div class="col-md-2">
                    <label>Judul: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $video->name }}" placeholder="Judul" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Link: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('link') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="link" value="{{ old('link') ? old('link') : $video->link }}" placeholder="Link" required>
                          <small class="text-danger">{{ $errors->first('link') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Caption: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <textarea class="form-control" name="description" placeholder="Caption">{{ old('description') ? old('description') : $video->description }}</textarea>
                          <small class="text-danger">{{ $errors->first('description') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Edit</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
