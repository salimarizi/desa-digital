@extends('layouts.backoffice_layout')

@section('css')
  <link rel="stylesheet" href="{!! asset('admin_layouts/plugins/select2/select2.min.css') !!}">
@endsection

@section('title')
  Home
@endsection

@section('sub_title')
  Edit Berita Desa
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Edit Berita Desa</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/news/'.$news->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                {{ method_field('PATCH') }}
                <div class="row">
                  <div class="col-md-2">
                    <label>Judul Berita Desa: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="title" value="{{ old('title') ? old('title') : $news->title }}" placeholder="Judul Berita Desa" required>
                          <small class="text-danger">{{ $errors->first('title') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Kategori: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
                          <select class="form-control" name="category_id" required>
                            @foreach ($news_categories as $category)
                              <option value="" disabled>Pilih Kategori</option>
                              @if ($category->id == $news->category_id)
                                <option value="{{ $category->id }}" selected>{{ $category->name }}</option>
                              @else
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                              @endif
                            @endforeach
                          </select>
                          <small class="text-danger">{{ $errors->first('category_id') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Konten: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                          <textarea class="form-control" name="content" placeholder="Konten">{{ old('content') ? old('content') : $news->content }}</textarea>
                          <small class="text-danger">{{ $errors->first('content') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Tag: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('tags') ? ' has-error' : '' }}">
                          <select class="form-control select2" id="tags_cb" name="tags[]" multiple>
                            @foreach ($tags as $tag)
                              @if (array_search($tag->id, $news_tags) !== false)
                                <option value="{{ $tag->id }}" selected>{{ $tag->name }}</option>
                              @else
                                <option value="{{ $tag->id }}">{{ $tag->name }}</option>
                              @endif
                            @endforeach
                          </select>
                          <small class="text-danger">{{ $errors->first('tags') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Gambar: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                          <input type="file" class="form-control" name="image">
                          <small class="text-danger">{{ $errors->first('image') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Edit</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection

@section('js')
  <script src="{!! asset('admin_layouts/plugins/select2/select2.full.min.js') !!}"></script>
  <script type="text/javascript">
    $('.select2').select2()

    $("#tags_cb").select2({
      placeholder : 'Pilih Tag, bisa lebih dari satu'
    })
  </script>
@endsection
