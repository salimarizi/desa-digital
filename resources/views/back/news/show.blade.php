@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Berita Desa
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Berita Desa</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-4">
                    @if ($news->image)
                      <img src="{{ url('storage/news/'.$news->image) }}" class="img img-responsive">
                    @else
                      <img src="{{ url('default_images/user.png') }}" class="img img-responsive">
                    @endif
                  </div>
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Judul Berita Desa </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $news->title }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Kategori </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $news->category->name }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Berita Desa </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $news->content }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Tag </label>
                      </div>
                      <div class="col-md-7">
                          <ul>
                            @foreach ($news_tags as $news)
                              <li>{{ $news->tag->name }}</li>
                            @endforeach
                          </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection
