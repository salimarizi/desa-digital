@extends('layouts.backoffice_layout')

@section('title')
  Harga
@endsection

@section('sub_title')
  Tambah Harga
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Form Harga</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/pricings') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                  <div class="col-md-2">
                    <label>Judul Harga: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="title" value="{{ old('title') }}" placeholder="Judul Harga" required>
                          <small class="text-danger">{{ $errors->first('title') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Kode Mata Uang: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('currency') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="currency" value="{{ old('currency') }}" placeholder="Kode Mata Uang" required>
                          <small class="text-danger">{{ $errors->first('currency') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Harga: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                          <input type="text" class="form-control number_only" name="price" value="{{ old('price') }}" placeholder="Harga" required>
                          <small class="text-danger">{{ $errors->first('price') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Fitur: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('features') ? ' has-error' : '' }}">
                          <textarea class="form-control" name="features" placeholder="Fitur (Pisahkan dengan koma). Ex: Support 24jam, full service, maintenance bulanan">{{ old('features') }}</textarea>
                          <small class="text-danger">{{ $errors->first('features') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
