@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Detail Harga
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Detail Harga</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
                <div class="row">
                  <div class="col-md-8">
                    <div class="row">
                      <div class="col-md-3">
                        <label>Judul Harga </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $pricing->title }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Harga </label>
                      </div>
                      <div class="col-md-7">
                          : {{ $pricing->currency }}{{ $pricing->price }}
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-3">
                        <label>Fitur </label>
                      </div>
                      <div class="col-md-7">
                          @php
                            $features = explode(',', $pricing->features);
                          @endphp
                          <ul>
                            @foreach ($features as $feature)
                              <li>{{ $feature }}</li>
                            @endforeach
                          </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
@endsection
