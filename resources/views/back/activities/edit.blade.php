@extends('layouts.backoffice_layout')

@section('title')
  Home
@endsection

@section('sub_title')
  Edit Aktivitas
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-10">
                    <h4>Edit Aktivitas</h4>
                  </div>
                  <div class="col-md-2"></div>
                </div>
              </div>
              <div class="box-body">
              <form action="{{ url('admin/activities/'.$activity->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                {{ method_field('PATCH') }}
                <div class="row">
                  <div class="col-md-2">
                    <label>Nama Aktivitas: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                          <input type="text" class="form-control" name="name" value="{{ old('name') ? old('name') : $activity->name }}" placeholder="Nama Aktivitas" required>
                          <small class="text-danger">{{ $errors->first('name') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Tanggal: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('date') ? ' has-error' : '' }}">
                          <input type="date" class="form-control" name="date" value="{{ old('date') ? old('date') : $activity->date }}" placeholder="Tanggal" required>
                          <small class="text-danger">{{ $errors->first('date') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Deskripsi: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                          <textarea class="form-control" name="description" placeholder="Deskripsi">{{ old('description') ? old('description') : $activity->description }}</textarea>
                          <small class="text-danger">{{ $errors->first('description') }}</small>
                      </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-2">
                    <label>Gambar: </label>
                  </div>
                  <div class="col-md-8">
                      <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                          <input type="file" class="form-control" name="image">
                          <small class="text-danger">{{ $errors->first('image') }}</small>
                      </div>
                  </div>
                </div>
              </div>

              <div class="box-footer">
                <button type="submit" class="btn btn-success pull-right">Edit</button>
                </form>
              </div>
          </div>
        </div>
      </div>
@endsection
