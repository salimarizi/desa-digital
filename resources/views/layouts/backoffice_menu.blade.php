<ul class="sidebar-menu">
  <li class="header">MENU</li>
  <!-- Optionally, you can add icons to the links -->
  <li>
    <a href="{{ url('/admin/home') }}"><i class="fa fa-dashboard"></i>
      <span>Dashboard</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/testimonies') }}"><i class="fa fa-comment"></i>
      <span>Testimoni</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/apps') }}"><i class="fa fa-archive"></i>
      <span>Aplikasi Desa</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/services') }}"><i class="fa fa-gear"></i>
      <span>Layanan</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/faqs') }}"><i class="fa fa-question"></i>
      <span>FAQ</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/partners') }}"><i class="fa fa-users"></i>
      <span>Mitra</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/tags') }}"><i class="fa fa-tag"></i>
      <span>Tag</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/news_categories') }}"><i class="fa fa fa-newspaper-o"></i>
      <span>Kategori Berita</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/news') }}"><i class="fa fa fa-newspaper-o"></i>
      <span>Berita</span>
    </a>
  </li>

  <li>
    <a href="{{ url('/admin/pricings') }}"><i class="fa fa fa-dollar"></i>
      <span>Harga</span>
    </a>
  </li>
</ul>
