<!DOCTYPE html>
<html lang="zxx">

<head>
  <meta charset="utf-8">
  <title>Desa Digital</title>

  <!-- mobile responsive meta -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

  <!-- ** Plugins Needed for the Project ** -->
  <!-- Bootstrap -->
  <link rel="stylesheet" href="{!! asset('frontend_assets/plugins/bootstrap/bootstrap.min.css') !!}">
	<!-- FontAwesome -->
  <link rel="stylesheet" href="{!! asset('frontend_assets/plugins/fontawesome/font-awesome.min.css') !!}">
	<!-- Animation -->
	<link rel="stylesheet" href="{!! asset('frontend_assets/plugins/animate.css') !!}">
	<!-- Prettyphoto -->
	<link rel="stylesheet" href="{!! asset('frontend_assets/plugins/prettyPhoto.css') !!}">
	<!-- Owl Carousel -->
	<link rel="stylesheet" href="{!! asset('frontend_assets/plugins/owl/owl.carousel.css') !!}">
	<link rel="stylesheet" href="{!! asset('frontend_assets/plugins/owl/owl.theme.css') !!}">
	<!-- Flexslider -->
	<link rel="stylesheet" href="{!! asset('frontend_assets/plugins/flex-slider/flexslider.css') !!}">
	<!-- Flexslider -->
	<link rel="stylesheet" href="{!! asset('frontend_assets/plugins/cd-hero/cd-hero.css') !!}">
	<!-- Style Swicther -->
	<link id="style-switch" href="{!! asset('frontend_assets/css/presets/preset3.css') !!}" media="screen" rel="stylesheet" type="text/css">

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
      <script src="plugins/html5shiv.js"></script>
      <script src="plugins/respond.min.js"></script>
    <![endif]-->

  <!-- Main Stylesheet -->
  <link href="{!! asset('frontend_assets/css/style.css') !!}" rel="stylesheet">

  <!--Favicon-->
	<link rel="icon" href="{!! asset('frontend_assets/img/favicon/favicon-32x32.png') !!}" type="image/x-icon" />
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="{!! asset('frontend_assets/img/favicon/favicon-144x144.png') !!}">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="{!! asset('frontend_assets/img/favicon/favicon-72x72.png') !!}">
	<link rel="apple-touch-icon-precomposed" href="{!! asset('frontend_assets/img/favicon/favicon-54x54.png') !!}">
  @yield('css')
</head>

<body>

	<div class="body-inner">

  <!-- Header start -->
  <header id="header" class="fixed-top header" role="banner">
  	<a class="navbar-brand navbar-bg" href="{{ url('/') }}">
      <img class="img-fluid" src="{!! asset('frontend_assets/images/logo.png') !!}" style="max-width: 120px; margin-top: -10px" alt="logo">
    </a>
  	<div class="container">
  		<nav class="navbar navbar-expand-lg navbar-dark">
  			<button class="navbar-toggler ml-auto border-0 rounded-0 text-white" type="button" data-toggle="collapse"
  				data-target="#navigation" aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
  				<span class="fa fa-bars"></span>
  			</button>

        <div class="collapse navbar-collapse text-center" id="navigation">
  				<ul class="navbar-nav ml-auto">
  					<li class="nav-itemactive">
  						<a class="nav-link" href="{{ url('profile') }}" role="button" aria-haspopup="true" aria-expanded="false">
  							Profile
  						</a>
  					</li>
            <li class="nav-item">
  						<a class="nav-link" href="{{ url('apps') }}" role="button" aria-haspopup="true" aria-expanded="false">
  							Aplikasi Desa
  						</a>
  					</li>
            <li class="nav-item">
  						<a class="nav-link" href="{{ url('services') }}" role="button" aria-haspopup="true" aria-expanded="false">
  							Layanan
  						</a>
  					</li>
            <li class="nav-item">
  						<a class="nav-link" href="{{ url('digital-concept') }}" role="button" aria-haspopup="true" aria-expanded="false">
  							Konsep Digital
  						</a>
  					</li>
            <li class="nav-item">
  						<a class="nav-link" href="{{ url('news') }}" role="button" aria-haspopup="true" aria-expanded="false">
  							Berita Desa
  						</a>
  					</li>
            <li class="nav-item">
  						<a class="nav-link" href="{{ url('contact') }}" role="button" aria-haspopup="true" aria-expanded="false">
  							Kontak
  						</a>
  					</li>
            <li class="nav-item">
  						<a class="nav-link" href="{{ url('tutorials') }}" role="button" aria-haspopup="true" aria-expanded="false">
  							Tutorial
  						</a>
  					</li>
  				</ul>
  			</div>
  		</nav>
  	</div>
  </header>
  <!--/ Header end -->

  @yield('content')

	<!-- Footer start -->
	<footer id="footer" class="footer">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-4 col-sm-12 footer-widget">
	        <h3 class="widget-title">Berita Terbaru</h3>
          @foreach ($recent_posts as $news)
            <div class="latest-post-items media">
              <div class="latest-post-content media-body">
                <h4><a href="#">{{ $news->title }}</a></h4>
                <p class="post-meta">
                  <span class="author">Posted by {{ $news->writer->name }}</span>
                  <span class="post-meta-cat">in<a href="#"> {{ $news->category->name }}</a></span>
                </p>
              </div>
            </div>
          @endforeach

	      </div>
	      <!--/ End Recent Posts-->


	      <div class="col-md-4 col-sm-12 footer-widget">
	        <h3 class="widget-title">Foto Kegiatan</h3>

	        <div class="img-gallery">
	          <div class="img-container">
              @foreach ($activities as $activity)
                <a class="thumb-holder" data-rel="prettyPhoto" href="{!! asset('storage/activities/'.$activity->image) !!}">
                  <img src="{!! asset('storage/activities/'.$activity->image) !!}" alt="">
                </a>
              @endforeach
	          </div>
	        </div>
	      </div>
	      <!--/ end flickr -->

	      <div class="col-md-3 col-sm-12 footer-widget footer-about-us">
	        <h3 class="widget-title">Desa Digital</h3>
	        <p>Desa digital adalah platform sistem informasi yang dapat membantu kebutuhan desa.</p>
	        <h4>Address</h4>
	        <p>Jl. Sariwangi 3</p>
	        <div class="row">
	          <div class="col-md-6">
	            <h4>Email:</h4>
	            <p>cs@desadigital.org</p>
	          </div>
	          <div class="col-md-6">
	            <h4>Telepon</h4>
	            <p>+(62) 22-220</p>
	          </div>
	        </div>
	        <form action="#" role="form">
	          <div class="input-group subscribe">
	            <input type="email" class="form-control" placeholder="Email Address" required="">
	            <span class="input-group-addon">
	              <button class="btn" type="submit"><i class="fa fa-envelope-o"> </i></button>
	            </span>
	          </div>
	        </form>
	      </div>
	      <!--/ end about us -->

	    </div><!-- Row end -->
	  </div><!-- Container end -->
	</footer><!-- Footer end -->


	<!-- Copyright start -->
	<section id="copyright" class="copyright angle">
	  <div class="container">
	    <div class="row">
	      <div class="col-md-12 text-center">
	        <ul class="footer-social unstyled">
	          <li>
	            <a title="Twitter" href="#">
	              <span class="icon-pentagon wow bounceIn"><i class="fa fa-twitter"></i></span>
	            </a>
	            <a title="Facebook" href="#">
	              <span class="icon-pentagon wow bounceIn"><i class="fa fa-facebook"></i></span>
	            </a>
	            <a title="Google+" href="#">
	              <span class="icon-pentagon wow bounceIn"><i class="fa fa-google-plus"></i></span>
	            </a>
	            <a title="linkedin" href="#">
	              <span class="icon-pentagon wow bounceIn"><i class="fa fa-linkedin"></i></span>
	            </a>
	            <a title="Pinterest" href="#">
	              <span class="icon-pentagon wow bounceIn"><i class="fa fa-pinterest"></i></span>
	            </a>
	            <a title="Skype" href="#">
	              <span class="icon-pentagon wow bounceIn"><i class="fa fa-skype"></i></span>
	            </a>
	            <a title="Dribble" href="#">
	              <span class="icon-pentagon wow bounceIn"><i class="fa fa-dribbble"></i></span>
	            </a>
	          </li>
	        </ul>
	      </div>
	    </div>
      <!--/ Row end -->
	    <div class="row">
	      <div class="col-md-12 text-center">
	        <div class="copyright-info">
	          Copyright &copy; PT. Altechno Digital Nusantara 2016
	        </div>
	      </div>
	    </div>
	    <!--/ Row end -->
	    <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix position-fixed">
	      <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i></button>
	    </div>
	  </div>
	  <!--/ Container end -->
	</section>
	<!--/ Copyright end -->

</div><!-- Body inner end -->

<!-- jQuery -->
<script src="{!! asset('frontend_assets/plugins/jQuery/jquery.min.js') !!}"></script>
<!-- Bootstrap JS -->
<script src="{!! asset('frontend_assets/plugins/bootstrap/bootstrap.min.js') !!}"></script>
<!-- Style Switcher -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/style-switcher.js') !!}"></script>
<!-- Owl Carousel -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/owl/owl.carousel.js') !!}"></script>
<!-- PrettyPhoto -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/jquery.prettyPhoto.js') !!}"></script>
<!-- Bxslider -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/flex-slider/jquery.flexslider.js') !!}"></script>
<!-- CD Hero slider -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/cd-hero/cd-hero.js') !!}"></script>
<!-- Isotope -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/isotope.js') !!}"></script>
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/ini.isotope.js') !!}"></script>
<!-- Wow Animation -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/wow.min.js') !!}"></script>
<!-- Eeasing -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/jquery.easing.1.3.js') !!}"></script>
<!-- Counter -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/jquery.counterup.min.js') !!}"></script>
<!-- Waypoints -->
<script type="text/javascript" src="{!! asset('frontend_assets/plugins/waypoints.min.js') !!}"></script>
<!-- google map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCcABaamniA6OL5YvYSpB3pFMNrXwXnLwU&libraries=places"></script>
<script src="{!! asset('frontend_assets/plugins/google-map/gmap.js') !!}"></script>

<!-- Main Script -->
<script src="{!! asset('frontend_assets/js/script.js') !!}"></script>
<script src="{!! asset('frontend_assets/js/slider.js') !!}"></script>
@yield('js')
</body>

</html>
