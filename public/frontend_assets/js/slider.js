$(document).ready(function() {
    index = 0
    loopSlider(index)
})


loopSlider = (index) => {
  setTimeout(function(){
    max = $('.cd-slider-nav nav ul').children().length - 1
    if (index < max) {
      index++
    }else {
      index = 0
    }
    loopSlider(index)
    $('.cd-slider-nav nav ul').children().eq(index).addClass('selected active')
    $('.cd-hero-slider').children().eq(index).addClass('selected active')

    if (index > 0) {
      index2 = index - 1
    }else {
      index2 = 3
    }
    $('.cd-slider-nav nav ul').children().eq(index2).removeClass('selected active')
    $('.cd-hero-slider').children().eq(index2).removeClass('selected active')
  }, 5000)
}
