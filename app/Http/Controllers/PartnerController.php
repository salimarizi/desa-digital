<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\Partner;

class PartnerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $partners = Partner::orderBy('id', 'desc');
        return DataTables::of($partners)
              ->addIndexColumn()
              ->addColumn('edit', function ($partner) {
                  $url = url('admin/partners/'.$partner->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($partner) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$partner->id.',\''.$partner->name.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($partner) {
                  $url = url('admin/partners/'.$partner->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.partners.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.partners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }

        Partner::create($data);
        return redirect('admin/partners');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        return view('back.partners.show', compact('partner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Partner $partner)
    {
        return view('back.partners.edit', compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Partner $partner)
    {
        $data = $request->all();

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }

        $partner->update($data);
        return redirect('admin/partners');
    }

    public function uploadImage(Request $request)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('storage/partners/'), $imageName);
        return $imageName;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Partner $partner)
    {
        $partner->delete();
        return redirect('admin/partners');
    }
}
