<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\Service;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $services = Service::orderBy('id', 'desc');
        return DataTables::of($services)
              ->addIndexColumn()
              ->addColumn('edit', function ($service) {
                  $url = url('admin/services/'.$service->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($service) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$service->id.',\''.$service->name.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($service) {
                  $url = url('admin/services/'.$service->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.services.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Service::create($request->all());
        return redirect('admin/services');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return view('back.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('back.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $service->update($request->all());
        return redirect('admin/services');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return redirect('admin/services');
    }
}
