<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Partner;
use App\Models\Apps;
use App\Models\Testimony;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $partners = Partner::all();
        $apps = Apps::all();
        $testimonies = Testimony::all();

        return view('back.dashboard', compact('partners', 'apps', 'testimonies'));
    }
}
