<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\Setting;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $settings = Setting::orderBy('id', 'desc');
        return DataTables::of($settings)
              ->addIndexColumn()
              ->editColumn('value', function ($setting) {
                  if (substr($setting->name, 0, 6) == 'slider') {
                    return '<img src="'.asset('storage/settings/'.$setting->value).'" class="img img-fluid" style="max-width: 200px">';
                  }
                  return $setting->value;
              })
              ->addColumn('edit', function ($setting) {
                  $url = url('admin/settings/'.$setting->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'value'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.settings.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        return view('back.settings.edit', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        $data = $request->all();

        if ($request->image) {
          $data['value'] = $this->uploadImage($request);
        }

        $setting->update($data);
        return redirect('admin/settings');
    }

    public function uploadImage(Request $request)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('storage/settings/'), $imageName);
        return $imageName;
    }
}
