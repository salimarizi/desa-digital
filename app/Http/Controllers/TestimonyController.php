<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\Testimony;

class TestimonyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $testimonies = Testimony::orderBy('id', 'desc');
        return DataTables::of($testimonies)
              ->addIndexColumn()
              ->addColumn('edit', function ($testimony) {
                  $url = url('admin/testimonies/'.$testimony->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($testimony) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$testimony->id.',\''.$testimony->name.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($testimony) {
                  $url = url('admin/testimonies/'.$testimony->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.testimonies.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.testimonies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }

        Testimony::create($data);
        return redirect('admin/testimonies');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Testimony $testimony)
    {
        return view('back.testimonies.show', compact('testimony'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Testimony $testimony)
    {
        return view('back.testimonies.edit', compact('testimony'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Testimony $testimony)
    {
        $data = $request->all();

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }

        $testimony->update($data);
        return redirect('admin/testimonies');
    }

    public function uploadImage(Request $request)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('storage/testimonies/'), $imageName);
        return $imageName;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Testimony $testimony)
    {
        $testimony->delete();
        return redirect('admin/testimonies');
    }
}
