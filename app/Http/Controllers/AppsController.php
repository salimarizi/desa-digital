<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;

use App\Models\Apps;
use App\Models\AppImage;

class AppsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $apps = Apps::orderBy('id', 'desc');
        return DataTables::of($apps)
              ->addIndexColumn()
              ->addColumn('edit', function ($apps) {
                  $url = url('admin/apps/'.$apps->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($apps) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$apps->id.',\''.$apps->name.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($apps) {
                  $url = url('admin/apps/'.$apps->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.apps.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.apps.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Apps::create($request->all());
        return redirect('admin/apps');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Apps $app)
    {
        return view('back.apps.show', compact('app'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Apps $app)
    {
        return view('back.apps.edit', compact('app'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Apps $app)
    {
        $app->update($request->all());
        return redirect('admin/apps');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Apps $app)
    {
        $app->delete();
        return redirect('admin/apps');
    }

    public function loadImageData(Apps $app)
    {
        $images = AppImage::where('app_id', $app->id)->orderBy('id', 'desc');
        return DataTables::of($images)
              ->addIndexColumn()
              ->editColumn('image', function ($images) {
                  return '<img src="'.url('storage/apps/'.$images->image).'" class="img img-responsive" style="max-width: 200px">';
              })
              ->addColumn('delete', function ($images) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$images->id.',\' \')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->rawColumns(['image', 'delete'])
              ->make(true);
    }

    public function addImage(Apps $app)
    {
        return view('back.apps.add_image', compact('app'));
    }

    public function storeImage(Request $request, Apps $app)
    {
        $image = $this->uploadImage($request);

        AppImage::create([
          'app_id' => $app->id,
          'image' => $image
        ]);

        return redirect('admin/apps/'.$app->id);
    }

    public function uploadImage(Request $request)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('storage/apps/'), $imageName);
        return $imageName;
    }

    public function deleteImage(Apps $app, AppImage $app_image)
    {
        $app_image->delete();
        return redirect('admin/apps/'.$app->id);
    }
}
