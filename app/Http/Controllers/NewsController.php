<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use Auth;

use App\Models\News;
use App\Models\Tag;
use App\Models\NewsCategory;
use App\Models\NewsTag;

class NewsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function loadData()
    {
        $news = News::orderBy('id', 'desc');
        return DataTables::of($news)
              ->addIndexColumn()
              ->editColumn('content', function ($news) {
                return str_limit($news->content, 60);
              })
              ->addColumn('edit', function ($news) {
                  $url = url('admin/news/'.$news->id.'/edit');
                  return '<a href="'.$url.'" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                  </a>';
              })
              ->addColumn('delete', function ($news) {
                  return '<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modal-delete" onclick="deleteModal('.$news->id.',\''.$news->title.'\')">
                    <i class="fa fa-trash"></i>
                  </button>';
              })
              ->addColumn('show', function ($news) {
                  $url = url('admin/news/'.$news->id);
                  return '<a href="'.$url.'" class="btn btn-primary">
                    <i class="fa fa-chevron-right"></i>
                  </a>';
              })
              ->rawColumns(['edit', 'delete', 'show'])
              ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('back.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $news_categories = NewsCategory::orderBy('name')->get();
        $tags = Tag::orderBy('name')->get();
        return view('back.news.create', compact('news_categories', 'tags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['writer_id'] = Auth::user()->id;

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }
        $news_id = News::create($data)->id;

        foreach ($data['tags'] as $tag_id) {
          NewsTag::create(compact('news_id', 'tag_id'));
        }
        return redirect('admin/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        $news_tags = NewsTag::where('news_id', $news->id)->get();
        return view('back.news.show', compact('news', 'news_tags'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $news_categories = NewsCategory::orderBy('name')->get();
        $tags = Tag::orderBy('name')->get();
        $news_tags = NewsTag::where('news_id', $news->id)->pluck('tag_id')->toArray();
        return view('back.news.edit', compact('news', 'news_categories', 'tags', 'news_tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {

        $data = $request->all();
        $data['writer_id'] = Auth::user()->id;

        if ($request->image) {
          $data['image'] = $this->uploadImage($request);
        }

        $news->update($data);
        NewsTag::where('news_id', $news->id)->delete();

        $news_id = $news->id;
        foreach ($data['tags'] as $tag_id) {
          NewsTag::create(compact('news_id', 'tag_id'));
        }
        return redirect('admin/news');
    }

    public function uploadImage(Request $request)
    {
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        request()->image->move(public_path('storage/news/'), $imageName);
        return $imageName;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->delete();
        return redirect('admin/news');
    }
}
