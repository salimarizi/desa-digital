<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Testimony;
use App\Models\Apps;
use App\Models\AppImage;
use App\Models\Partner;
use App\Models\Faq;
use App\Models\News;
use App\Models\NewsCategory;
use App\Models\Tag;
use App\Models\Service;
use App\Models\Video;
use App\Models\Feature;
use App\Models\Activity;
use App\Models\Pricing;
use App\Models\Setting;

class FrontController extends Controller
{
    public function index()
    {
        $testimonies = Testimony::all();
        $partners = Partner::all();
        $news = News::orderBy('id', 'desc')->limit(4)->get();

        $apps = Apps::orderBy('created_at', 'desc')->get();
        foreach ($apps as $app) {
          $app->image = AppImage::where('app_id', $app->id)->orderBy('id', 'desc')->first();
        }

        $features = Feature::orderBy('id', 'desc')->get();

        $services = Service::all();

        $videos = Video::orderBy('id', 'desc')->get();
        foreach ($videos as $video) {
          $start = strpos($video->link, "v=") + 2;
          $end = strpos($video->link, "&");

          $video->code = substr($video->link, $start, ($end - $start));
        }

        $pricings = Pricing::all();

        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        $slider_1 = Setting::where('name', 'slider_1')->first()->value;
        $slider_2 = Setting::where('name', 'slider_2')->first()->value;
        $slider_3 = Setting::where('name', 'slider_3')->first()->value;
        $slider_4 = Setting::where('name', 'slider_4')->first()->value;

        $desa_mitra = Setting::where('name', 'desa_mitra')->first()->value;
        $bumdes_mitra = Setting::where('name', 'bumdes_mitra')->first()->value;
        $proyek_desa = Setting::where('name', 'proyek_desa')->first()->value;
        $petani_binaan = Setting::where('name', 'petani_binaan')->first()->value;


        return view('front.index',
          compact(
            'testimonies', 'partners', 'news', 'apps', 'services', 'videos',
            'features', 'recent_posts', 'activities', 'pricings', 'slider_1',
            'slider_2', 'slider_3', 'slider_4', 'desa_mitra', 'bumdes_mitra',
            'proyek_desa', 'petani_binaan'
          )
        );
    }

    public function profile()
    {
        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.profile', compact('recent_posts', 'activities'));
    }

    public function services()
    {
        $testimonies = Testimony::all();
        $partners = Partner::all();
        $services = Service::all();
        $pricings = Pricing::all();

        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.services',
          compact('testimonies', 'partners', 'services', 'recent_posts',
          'activities', 'pricings')
        );
    }

    public function apps()
    {
        $apps = Apps::orderBy('created_at', 'desc')->get();
        foreach ($apps as $app) {
          $app->images = AppImage::where('app_id', $app->id)->orderBy('id', 'desc')->get();
        }

        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.apps', compact('apps', 'recent_posts', 'activities'));
    }

    public function digitalConcept()
    {
        $faqs = Faq::orderBy('id', 'desc')->get();

        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.digital-concept', compact('faqs', 'recent_posts', 'activities'));
    }

    public function news()
    {
        $news = News::orderBy('id', 'desc')->get();
        $news_categories = NewsCategory::orderBy('id', 'desc')->get();
        $tags = Tag::orderBy('id', 'desc')->get();

        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.news', compact('news', 'news_categories', 'tags', 'recent_posts', 'activities'));
    }

    public function newsDetail(News $news_detail)
    {
        $news_categories = NewsCategory::orderBy('id', 'desc')->get();
        $tags = Tag::orderBy('id', 'desc')->get();

        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.news-detail', compact('news_detail', 'news_categories', 'tags', 'recent_posts', 'activities'));
    }

    public function contact()
    {
        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.contact', compact('recent_posts', 'activities'));
    }

    public function tutorials()
    {
        $videos = Video::orderBy('id', 'desc')->get();
        foreach ($videos as $video) {
          $start = strpos($video->link, "v=") + 2;
          $end = strpos($video->link, "&");

          $video->code = substr($video->link, $start, ($end - $start));
        }

        // Footer Content
        $recent_posts = News::orderBy('id', 'desc')->paginate(3);
        $activities = Activity::orderBy('id', 'desc')->paginate(9);
        // End Footer Content

        return view('front.videos', compact('recent_posts', 'activities', 'videos'));
    }
}
