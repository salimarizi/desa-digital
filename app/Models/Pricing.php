<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
    protected $table = 'pricings';

    protected $fillable = [
        'title',
        'currency',
        'price',
        'features'
    ];

    public static function toNumeric($string)
    {
        return (double)str_replace('.', '', $string);
    }
}
