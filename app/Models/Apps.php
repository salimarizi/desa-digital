<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Apps extends Model
{
    protected $table = 'apps';

    protected $fillable = [
      'name',
      'feature',
      'description',
      'client',
      'link'
    ];
}
