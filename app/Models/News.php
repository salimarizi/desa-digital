<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = [
      'category_id',
      'writer_id',
      'title',
      'content',
      'image'
    ];

    public function category()
    {
        return $this->belongsTo(NewsCategory::class, 'category_id');
    }

    public function writer()
    {
        return $this->belongsTo(\App\User::class, 'writer_id');
    }

    public function news_tags()
    {
        return $this->hasMany(NewsTag::class, 'news_id');
    }
}
