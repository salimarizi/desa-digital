<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';

    protected $fillable = [
      'news_id',
      'name',
      'email',
      'message',
      'comment_tail'
    ];

    public function news()
    {
        return $this->belongsTo(News::class, 'news_id');
    }
}
