<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppImage extends Model
{
    protected $table = 'app_images';

    protected $fillable = [
      'app_id',
      'image'
    ];

    public function apps()
    {
        return $this->belongsTo(Apps::class, 'app_id');
    }
}
