<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontController@index');

Route::get('/profile', 'FrontController@profile');

Route::get('/apps', 'FrontController@apps');

Route::get('/services', 'FrontController@services');

Route::get('/digital-concept', 'FrontController@digitalConcept');

Route::get('/news', 'FrontController@news');

Route::get('/news-detail/{news_detail}', 'FrontController@newsDetail');

Route::get('/contact', 'FrontController@contact');

Route::get('/tutorials', 'FrontController@tutorials');


Auth::routes();
Route::prefix('admin')->prefix('admin')->group(function () {
  Route::get('/home', 'HomeController@index')->name('home');

  Route::get('testimonies/data', 'TestimonyController@loadData');
  Route::resource('testimonies', 'TestimonyController');

  Route::get('apps/data', 'AppsController@loadData');
  Route::get('apps/images/{app}', 'AppsController@loadImageData');
  Route::get('apps/{app}/add_image', 'AppsController@addImage');
  Route::post('apps/{app}/image', 'AppsController@storeImage');
  Route::delete('apps/{app}/image/{app_image}', 'AppsController@deleteImage');
  Route::resource('apps', 'AppsController');

  Route::get('services/data', 'ServiceController@loadData');
  Route::resource('services', 'ServiceController');

  Route::get('faqs/data', 'FaqController@loadData');
  Route::resource('faqs', 'FaqController');

  Route::get('partners/data', 'PartnerController@loadData');
  Route::resource('partners', 'PartnerController');

  Route::get('tags/data', 'TagController@loadData');
  Route::resource('tags', 'TagController');

  Route::get('news_categories/data', 'NewsCategoryController@loadData');
  Route::resource('news_categories', 'NewsCategoryController');

  Route::get('news/data', 'NewsController@loadData');
  Route::resource('news', 'NewsController');

  Route::get('videos/data', 'VideoController@loadData');
  Route::resource('videos', 'VideoController');

  Route::get('activities/data', 'ActivityController@loadData');
  Route::resource('activities', 'ActivityController');

  Route::get('features/data', 'FeatureController@loadData');
  Route::resource('features', 'FeatureController');

  Route::get('pricings/data', 'PricingController@loadData');
  Route::resource('pricings', 'PricingController');

  Route::get('settings/data', 'SettingController@loadData');
  Route::resource('settings', 'SettingController');
});
